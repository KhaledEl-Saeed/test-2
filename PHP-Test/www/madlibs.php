<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <form action="madlibs.php" method="post">
            color:
            <input type="text" name="color">
            <br>
            plural Nour:
            <input type="text" name="pluralNoun">
            <br>
            Celebrity:
            <input type="text" name="celebrity">

            <input type="submit">
        </form>
        <br>
            <?php
                $color = $_POST["color"];
                $pluralNoun = $_POST["pluralNoun"];
                $celebrity = $_POST["celebrity"];
                echo "Roses are $color <br>";
                echo " $pluralNoun  are blue <br>";
                echo "I love $celebrity <br>";
            ?>
    </body>
</html>