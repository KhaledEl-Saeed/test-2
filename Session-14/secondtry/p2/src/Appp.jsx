import React from 'react';
import Header from './Components/Header';
import About from './Components/About';
import Contact from './Components/Contact';
import Footer from './Components/Footer';
import "./try.css";
import "bootstrap/dist/css/bootstrap.min.css";


const Appp = () => {
  return <div >
        <Header/>
        <About/>
        <Contact/>
        <Footer/>
  </div>
};

export default Appp;
   