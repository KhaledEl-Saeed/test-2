import React from 'react';

const About = ()=>(
   <section className='bg-color padding'>
       <section className='container'>
            <section className='row justify-content-center'>
                <section className=' col-8 text-center'>
                    <h1 className='f-85'> About</h1>
                    <p className='pt-5'>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Voluptate aut at veritatis officia hic earum veniam dolorem inventore architecto consectetur.
                    </p>
                </section>
            </section>

            <section className='row justify-content-center pt-5'>
                <section className='col-4 text-center'>
                    <section className='card-deck'>
                        <section className='card'>
                            <section className='card-body pt-3 pb-5'>
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                            </section>
                        </section>
                    </section>
                </section>
                <section className='col-4 text-center'>
                    <section className='card-deck'>
                        <section className='card'>
                            <section className='card-body pt-3 pb-5'>
                                    <h4 class="card-title">Card title</h4>
                                    <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                            </section>
                        </section>
                    </section>
                </section>
                <section className='col-4 text-center'>
                    <section className='card-deck'>
                        <section className='card'>
                            <section className='card-body pt-3 pb-5'>
                                    <h4 className='card-title'>Card title</h4>
                                    <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Explicabo provident iste animi repellendus porro doloremque error sit est fuga magnam!</p>
                            </section>
                        </section>
                    </section>
                </section>
                <button className='button mt-5'>
                    Click Here!
                </button>  
            </section>

            

   </section>

   </section>
   
   

);

export default About;