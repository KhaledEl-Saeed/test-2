import React from 'react';

const Contact = () => (
    <section className="pb-5 pt-5 blue-bg">
        <section className='container'>
            <section className='row justify-content-center font-color'>
                <section className='col-8 text-center'>
                    <h1 className='f-85'>Contact Us</h1>
                    <p className='pb-5 pt-3'>
                    For any inquiries message us
                    </p>

                </section>
            </section>
            <section className='row justify-content-center'>
                <section className='col-6 '>
                    <section className='form-group  '>
                    <input type="text" className="form-control mb-5 p-4" id="formGroupExampleInput" placeholder="Enter your Name"/>
                    <input type="text" className="form-control mb-5 p-4" id="formGroupExampleInput" placeholder="Enter your E-mail"/>
                    <input type="text" className="form-control mb-5 p-4" id="formGroupExampleInput" placeholder="Enter your Phone Number"/>
                    </section>
                </section>
                <section className='col-6  d-flex flex-stretch'>
                    <textarea className='form-control' placeholder="Enter your Message">
                    </textarea>

                </section>
                <button className=' button-2 btn-light mt-5 '>
                    Click Here!
                </button> 

            </section>
        </section>
    </section>
    
    
);



export default Contact;