import React from 'react';

const Footer = () => (
    <footer>
        <section className='container'>
            <section className='row pt-5'>
                <section className='col-4'>
                    <ul>
                        <li><a href="#">link</a></li>
                        <li><a href="#">link</a></li>
                        <li><a href="#">link</a></li>
                    </ul>
                </section>
                <section className='col-4'>
                    <ul>
                        <li><a href="#">link</a></li>
                        <li><a href="#">link</a></li>
                        <li>l<a href="#">link</a></li>
                    </ul>
                </section>
                <section className='col-4'>
                    <ul>
                        <li><a href="#">link</a></li>
                        <li><a href="#">link</a></li>
                        <li><a href="#">link</a></li>
                    </ul>
                </section>


            </section>
        </section>
    </footer>

);

export default Footer;