import React from 'react';

const Header = () => (
    <header className='bg'>
        <section className='container'>
            <section className='row'>
                <section className="col-xl-3 col-lg-3  col-md-3">
                        <a class="navbar-brand disabled font-color" href="#!">Brand</a>
                </section>  
                <section className='col-xl-9 col-lg-9 col-md-9'>
                <nav className="navbar  navbar-expand-sm ">
                    <ul className="navbar-nav ml-auto font-color ">
                        <li className="nav-link">Home</li>
                        <li className="nav-link">About</li>
                        <li className="nav-link">Contact</li>
                    </ul>     
                </nav>
            </section>
            </section>
         </section>  
    </header>
);

export default Header;