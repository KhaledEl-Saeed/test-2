import React from 'react';
import ReactDOM from 'react-dom';
import Appp from './Appp';
import './index.css';
import * as serviceWorker from './serviceWorker';




ReactDOM.render(
  <Appp/>, 
  document.getElementById('root')
  );
//document.getElementById('root').innerHTML = '';
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
