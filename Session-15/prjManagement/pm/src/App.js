import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";

import ProjectList from './Components/ProjectList';

/*function App() {
  return (
      <section className="App">
        <section className="container">
          <h1>Project Managment</h1>
          <ul>
            <li><a href='#'>LMS</a> <button>Edit</button> <button>Delete</button></li>
            <li><a href='#'>CRM</a><button>Edit</button> <button>Delete</button></li>
            <li><a href='#'>LMS</a><button>Edit</button> <button>Delete</button></li>
          </ul>

        </section>

      </section>
  );
}*/

class App extends React.Component{

  state = {
    projects : [
      {
        id : 1,
        title : 'LMS',
      },
      {
        id :2,
        title : 'CMS',
      },
      {
        id: 3,
        title: 'CRM',
      },
      {
        id: 4,
        title: 'DataBase'
      }
     ],
     newProject :''

  };

  changeHandler = e => {  //as2l 3leha
   const newProject = e.target.value;
    console.log(e.target.value)
    this.setState({
    newProject
    }); 
  };

  

  onClickNewProjectHandle = () => {
    const projects = this.state.projects;
    projects.push({
       id: projects.length +1,
       title: this.state.newProject
    });
    this.setState({
      projects,
      newProject: ''
    });
  }

  onClickDeleteProjectHandle = project => {
    let projects = this.state.projects.filter( rowProject =>{
        return rowProject !== project;
    })
    this.setState({
      projects
    });
  };

  

 

  onClickProjectHandle = () =>{  
    console.log(this.state)
  }

  render(){
    return(
      <section className="App">
        <section className="container">
          <h1>Project Managment</h1>
          <section className="add-new form-group text-center">
            
            <input type="text"
                   value={this.state.newProject} 
                   onChange={this.changeHandler}
                   className="form-control m-2"/>
            <button className="btn btn-success btn-block "
                onClick={this.onClickNewProjectHandle}>Add New</button>
          </section>
          <ProjectList projects={this.state.projects} 
                       deleteProject={this.onClickDeleteProjectHandle}
                       
                       />

        </section>

      </section>
      


    )
  }
}

export default App;
