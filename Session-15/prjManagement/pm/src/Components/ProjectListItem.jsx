import React from 'react';

export class ProjectListItem extends React.Component{

    state = {
        project:'',
        editable : false

    };

    onClickEditButton = () => {
        this.setState({
            editable : true,
            project: this.props.project.title
        });
    };
    onClickSaveButton = () => {
        const UpdatedProject = this.props.project;
        UpdatedProject.title = this.state.project;
        
        this.setState({
            editable : false,
            project: ''
        });
    };

    changeHandler = e => {  
        const project = e.target.value;
         console.log(e.target.value)
         this.setState({
            project
         }); 
       };
    
    render(){
        
        return(
            <li className="list-group-item ">
                <a href='#' className="btn mr-2">{
                this.state.editable?<input type="text" 
                                           onChange={this.changeHandler}
                                           value={this.state.project}/>
                                           :this.props.project.title}
                </a> 
                {this.state.editable ? (<button className="btn btn-primary mr-2 float-right"
                                        onClick={this.onClickSaveButton} >
                    Save
                </button> ):(
                    <button className="btn btn-info mr-2 float-right" 
                    onClick={this.onClickEditButton}>
                        Edit
                    </button> 
                )}



                
                <button className="btn btn-danger mr-2 float-right" 
                        onClick={() =>this.props.deleteProject(this.props.project)}>  
                    Delete
                </button>
            </li>

        );
    }

}



export default ProjectListItem;