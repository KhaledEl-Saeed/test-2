import React from 'react';
import "bootstrap/dist/css/bootstrap.min.css";

import NavBar from './Layout/NavBar';
import AddListView from './Components/AddListView';
import ListView from './Components/ListView';


import {BrowserRouter, Route, Switch} from 'react-router-dom';

import './App.css';




class App extends React.Component {
  state = {
    Items : [
        {
          id : 1,
          name : 'item 1',
          price : 50
        },
        {
          id :2,
          name : 'Item 2',
          price : 50
        },
        {
          id: 3,
          name: 'Item 3',
          price : 50
        },
        {
          id: 4,
          name: 'Item 4',
          price : 50
        }
       ],
       newItem :''

};

changeHandlerName = e => {  
  const newName = e.target.value;
   console.log(e.target.value);
   this.setState({
   newName
   }); 
 };
 changeHandlerPrice = e => {  
  const newPrice = e.target.value;
  console.log(e.target.value);
   this.setState({
    newPrice
   }); 
 };

 onClickNewItemHandle = () => {
   const Items = this.state.Items;
   
   Items.push({
     id: Items.length +1,
     name: this.state.newName,
     price : this.state.newPrice
   });
   this.setState({
     Items,
     newItem:''
   })
 }

 onClickDeleteItemHandle = () => {
   let Items = this.state.Items.filter(rowItem =>{
     return rowItem !==Items;
   })
   this.setState({
     Items
   });
 };

  render(){
    return (
      <section className="App">
          <BrowserRouter>
          <NavBar/>
          <section className="container">
            <Switch>
                
             <Route exact path="/">
              <section className="text-center">
                <h1 className="pt-5">Expenses Calculator</h1>
                <h3 className="pt-5">Your total expenses are 0 EGP</h3>
                <h3 className="pt-5">You have 0 items in your expenses list</h3>
              </section>
              </Route>

              <Route exact path="/AddListView">
                  <AddListView  newItem={this.state.newItem}
                                changeHandlerPrice={this.changeHandlerPrice}
                                changeHandlerName={this.changeHandlerName}
                                onClickNewItemHandle={this.onClickNewItemHandle}/>   
              </Route>

              <Route exact path="/ListView">
              <ListView Items={this.state.Items}
                        deleteItem = {this.onClickDeleteItemHandle}/>
              </Route>

            </Switch>
          
    
        </section>
        </BrowserRouter>
    </section>
      
      
      
    );
  }
}

export default App;
