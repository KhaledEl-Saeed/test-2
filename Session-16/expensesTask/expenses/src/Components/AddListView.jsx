import React from 'react';



export class AddListView extends React.Component{
    
    
    
    render(){
        
        return(
             <React.Fragment>
                 <section className="container "> 
                <h1 className="Text-center pt-5">Add a new Item</h1>
                <section className="form-group">
                    <label className="float-left">Item</label>
                    <input type="text" className="form-control" value={this.props.newName}
                                                                onChange={this.props.changeHandlerName} ></input>
                </section>
                <section className="form-group">
                    <label className="float-left">Price</label>
                    <input type="number" className="form-control" value={this.props.newPrice}
                                                                onChange={this.props.changeHandlerPrice}></input>
                    
                </section>
                <button className="btn btn-success float-left" onClick={this.props.onClickNewItemHandle}>Add Item</button>

            </section>

             </React.Fragment>   
                 
        );
    }

}



export default AddListView;