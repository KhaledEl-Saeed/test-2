import React from 'react';




export const ListView = props =>(

    

       /* <section className="pt-5 text-center">
            <h1>You haven't added any expenses yet</h1>
        </section>*/
        <section className="container">
            <h1 className="pt-5 text-center">Expenses List</h1>
            <hr/>
            <table className="table ">
                <thead>
                    <tr>
                    <th >#</th>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Action</th>
                    </tr>
                </thead>
                
               

                <tbody>
                {props.Items.map(item => (
                                    <tr key={item.id}>
                                        <th scope="row">{item.id}</th>
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td><button type="button" className="btn btn-danger"
                                         onClick={() =>this.props.deleteItem(this.props.deleteItem)}>Delete</button></td>
                                    </tr>
                                    ))
                            }   
                </tbody>
   
            </table>
             
        </section>
 
);

export default ListView;









