import React from 'react';
import {Link , NavLink} from 'react-router-dom'



const NavBar = () => (
    
 <nav className="navbar navbar-expand-lg navbar navbar-dark bg-dark">
     
     <NavLink className="navbar-brand" to="/">Expenses</NavLink>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item ">
          <NavLink to="ListView" className="nav-link ">
            List
          </NavLink>
      </li>

      <li className="nav-item ">
          <NavLink to="AddListView" className="nav-link ">
            Add New
          </NavLink>
      </li>
      
    </ul>
  </div>

     

</nav>
);

export default NavBar;