import React from 'react';

import NavBar from './Layout/NavBar';
import Top from './Components/Top';
import Products from './Components/Products';
import Cart from './Components/Cart';
import Orders from './Components/Orders';
import Footer from './Layout/Footer';

import {BrowserRouter, Route, Switch} from 'react-router-dom';

import "bootstrap/dist/css/bootstrap.min.css";


class App extends React.Component {


state = {
  products:[
   
  ]
};


onChangeHandle = e =>{
  const name= e.target.value;
  this.setState({
    name
  })
}

onClickAddToCart = () =>{
  const products = this.state.products;

  products.push({
    id : products.length + 1,
    name : this.state.name,
    price : this.state.price,
    image : this.state.image

  });
  this.setState({
    products
   
  })
  console.log(products);
}








  render(){
    return (
      <section className="App">

        <BrowserRouter>

          <NavBar/>

            <Switch>

              <Route exact path="/">
                <Top onClickAddToCart = {this.onClickAddToCart}
                     onChangeHandle={this.onChangeHandle}/>
              </Route>

              <Route exact path="/Products"> 
                <Products/>
              </Route>

              <Route exact path="/Cart">
                <Cart/>
              </Route>

              <Route exact path="/Orders">
                <Orders/>
              </Route>  

            </Switch>

          <Footer/>

        </BrowserRouter>
                    
      </section>
    );
  }
  
}

export default App;
