import React from 'react';

export const Cart = ()=> (
    <section className="container text-center">
        <h1 className="mb-3">Cart</h1>
        <h5 className="mt-5">You have a total of 0 items in your cart</h5>
        <h2 className="mt-5">You don't have any products in your cart</h2>

        <hr/>
        <p>Total Price: 0 EGP</p>
        <section className="text-center">
            <button className="btn btn-success" disabled=""> Place Order </button>
        </section>

    </section>

)

export default Cart;