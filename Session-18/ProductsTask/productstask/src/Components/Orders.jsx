import React from 'react';

export const Orders = ()=> (
    <section className="container text-center">
        <h1 className="mb-3">Orders</h1>
        <h5 className="mt-5">You have a total of 0 orders placed</h5>
        <h2 className="mt-5">You don't have any orders yet</h2>

    </section>

)

export default Orders;