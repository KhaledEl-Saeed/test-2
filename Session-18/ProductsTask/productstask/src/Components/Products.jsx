import React from 'react';

export class Products extends React.Component{

    render(){
        return(
            <React.Fragment>
                <section className="container-fluid">
                    <section>
                        <h1 className="text-center">Products</h1>
                    </section>
                    <section className="row">
                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-1.jpg"  alt="product 1" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Camera Lens</h5>
                                    <p className="card-text">500 EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-2.jpg"  alt="product 2" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Smart Phone</h5>
                                    <p className="card-text">7000   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-3.jpg"  alt="product 3" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Nikon Black and Silver Camera</h5>
                                    <p className="card-text">9500   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-4.jpg"  alt="product 4" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Canon DSLR Camera</h5>
                                    <p className="card-text">8450   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-5.jpg"  alt="product 5" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Nintendo Game Boy</h5>
                                    <p className="card-text">700   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-6.jpg"  alt="product 6" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">FujiFilm DSLR Camera</h5>
                                    <p className="card-text">7500   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-7.jpg"  alt="product 7" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Lighter</h5>
                                    <p className="card-text">200   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-8.jpg"  alt="product 8" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">RED Dragon Video Camera</h5>
                                    <p className="card-text">200000   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-9.jpg"  alt="product 9" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Electric Keyboard</h5>
                                    <p className="card-text">5000   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-10.jpg"  alt="product 10" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Espresso Coffee Maker</h5>
                                    <p className="card-text">6450   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-11.jpg"  alt="product 11" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Canon Video Camera</h5>
                                    <p className="card-text">150000   EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        <section className="col-md-4">
                            <section className="card">
                                <img className="card-img-top" src="images/product-12.jpg"  alt="product 12" ></img>
                                <section className="card-body">
                                    <h5 className="card-title">Perfume</h5>
                                    <p className="card-text">1200    EGP</p>
                                    <button className="btn btn-primary" type="button"> Add To Cart</button>
                                </section>          
                            </section>
                        </section>

                        

                        

                        

                    </section>

                </section>
                 

            </React.Fragment>
        )
    }

}

export default Products;