import React from 'react';

export class Top extends React.Component{
    
    
    
    render(){
        
        return(
            <React.Fragment>
                <section className="text-center">
                    <h1>Top Products</h1>
                </section>
                <section className="row">
                    <section className="col-md-4">
                        <section className="card">
                            <img className="card-img-top" src="images/product-4.jpg"  alt="product 4" ></img>
                            <section className="card-body">
                                <h5 className="card-title" value={this.props.name} onChange={this.props.onChangeHandle}>Canon DSLR Camera</h5>
                                <p className="card-text">8450 EGP</p>
                                <button className="btn btn-primary" type="button" onClick={this.props.onClickAddToCart}> Add To Cart</button>
                            </section>          
                        </section>
                    </section>

                    <section className="col-md-4">
                        <section className="card">
                            <img className="card-img-top" src="images\product-5.jpg" alt="product 5"></img>
                            <section className="card-body">
                                <h5 className="card-title">Nintendo Game Boy</h5>
                                <p className="card-text">700 EGP</p>
                                <button className="btn btn-primary" type="button" onClick={this.props.onClickAddToCart}> Add To Cart</button>
                            </section>          
                        </section>
                    </section>

                    <section className="col-md-4">
                        <section className="card">
                            <img className="card-img-top" src="images\product-6.jpg" alt="product 6"></img>
                            <section className="card-body">
                                <h5 className="card-title">FujiFilm DSLR Camera</h5>
                                <p className="card-text">7500  EGP</p>
                                <button className="btn btn-primary" type="button"> Add To Cart</button>
                            </section>          
                        </section>
                    </section>

                </section>
                 

            </React.Fragment>
           

            
                 
        );
    }

}



export default Top;
