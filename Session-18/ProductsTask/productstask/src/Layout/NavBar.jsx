import React from 'react';
import { NavLink } from 'react-router-dom';




const NavBar = () => (
    
 <nav className="navbar navbar-expand-lg navbar navbar-light bg-light">
     
     <NavLink className="navbar-brand" to="/">P-STORE</NavLink>
  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>

  <div className="collapse navbar-collapse" id="navbarSupportedContent">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item ">
          <NavLink to="/Products" className="nav-link ">
            Products
          </NavLink>
      </li>

      <li className="nav-item ">
          <NavLink to="/Cart" className="nav-link ">
            Cart(0)
          </NavLink>
      </li>
      
      <li className="nav-item ">
          <NavLink to="/Orders" className="nav-link ">
            Orders
          </NavLink>
      </li>
    </ul>
  </div>

     

</nav>
);

export default NavBar;
