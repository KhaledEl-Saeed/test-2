<?php


$dsn = 'mysql:dbname=test2;host=localhost';
$username = 'root'; 
$password = '';



try {
    $dbh = new PDO($dsn, $username, $password);
    //step1: write query in var
    $sql = 'SELECT * FROM `clients`';
    //step2: prepare query
    $stm = $dbh->prepare($sql);
    //step3: execute prepared query
    $stm->execute();
    //step4: (optional) in insert, update, delete, required in select
    $data = $stm->fetchAll(PDO::FETCH_ASSOC);

    dump($data);

} catch (PDOException $e) {
    $message = date('Y-m-d H:i:s') . ': ' . $e->getMessage() . "\n";
    file_put_contents('error-pdo-connection.txt', $message, FILE_APPEND | LOCK_EX);
    die('Site down');

}
