<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once 'app/config/db_connection.php';


require_once 'app/model/User.class.php';
require_once 'app/model/Student.class.php';
require_once 'app/model/Teacher.class.php';







if(empty($_GET['p'])){ 
    $page = 'index';
} else{
    $page = $_GET['p'];
}

switch ($page){
    case 'home':
        require_once 'pages/home.view.php';
    break;

    case 'index':
        require_once 'pages/index.view.php';
    break;

    default:
        require_once 'pages/404.view.php';
}