@csrf
<div class="form-group">
<label for="name">Name</label>
<input type="text" name="name" id="name" class="form-control" value="{{ isset($product)?$product->name : '' }}">
        @if($errors->first('name'))
<br>
        <span class="alert alert-danger">
            {{ $errors->first('name')}}
        </span>
         @endif
    </div>

    <div class="form-group">
<label for="price">price</label>
<input type="text" name="price" id="price" class="form-control" value="{{ isset($product)?$product->price : '' }}">
        @if($errors->first('price'))
<br>
        <span class="alert alert-danger">
            {{ $errors->first('price')}}
        </span>
         @endif
    </div>  

    <div class="form-group">
<label for="quantity">quantity</label>
<input type="text" name="quantity" id="quantity" class="form-control" value="{{ isset($product)?$product->quantity : '' }}">
        @if($errors->first('quantity'))
<br>
        <span class="alert alert-danger">
            {{ $errors->first('quantity')}}
        </span>
         @endif
    </div>

    <div class="form-group">
<label for="image">image</label>
<input type="text" name="image" id="image" class="form-control" value="{{ isset($product)?$product->image : '' }}">
        @if($errors->first('image'))
<br>
        <span class="alert alert-danger">
            {{ $errors->first('image')}}
        </span>
         @endif
    </div>   

    <div class="form-group">
<label for="category_id">category_id</label>
<input type="text" name="category_id" id="category_id" class="form-control" value="{{ isset($product)?$product->category_id : '' }}">
        @if($errors->first('category_id'))
<br>
        <span class="alert alert-danger">
            {{ $errors->first('category_id')}}
        </span>
         @endif
    </div>


 <div class="form-group">
<label for="user_id">user_id</label>
<input type="text" name="user_id" id="user_id" class="form-control" value="{{ isset($product)?$product->user_id : '' }}">
        @if($errors->first('user_id'))
<br>
        <span class="alert alert-danger">
            {{ $errors->first('user_id')}}
        </span>
         @endif
    </div>         
     
<div class="form-group">
<label for="description">Description</label>
    <textarea name="description" id="description" cols="30" rows="10" class="form-control">
        {{ isset($product)?$product->description : '' }}
    </textarea>
@if($errors->first('description'))
<br>
<span class="alert alert-danger">
    {{ $errors->first('description')}}
</span>
@endif
</div>
<div class="text-center">
    <button type="submit" class="btn btn-success">Save</button>
</div>




                      