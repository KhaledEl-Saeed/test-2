<?php
  
    require_once "db.php";
    $message = '';
    if(isset($_POST['name']) && isset($_POST['email'])){
       $name = $_POST['name'];
       $email = $_POST['email'];
    }
    $sql = 'INSERT INTO students(name, email ) VALUES(:name, :email)';
    $statement = $dbh->prepare($sql);
    if($statement->execute([':name' => $name, ':email' => $email])){
        $message = 'data inserted successfully';    
    }
    
?>



<?php require_once "header.php" ?>

<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            <h2>Create a Sudent</h2>
        </div>
        <div class="card-body">
            <?php if(!empty($message)): ?>
                <div class="alert alert-success">
                    <?php echo $message?>
                </div>
            <?php endif; ?>

            <form method="post">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">email</label>
                    <input type="email" name="email" id="email" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info"> Create Student</button>
                </div>

            </form>
        </div>
    </div>
</div>


 <?php require_once "footer.php" ?>