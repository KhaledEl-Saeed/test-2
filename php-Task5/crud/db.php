<?php


$dsn = 'mysql:host=localhost;dbname=lms';
$username = 'root'; 
$password = '';



try {
    $dbh = new PDO($dsn, $username, $password);
    
} catch (PDOException $e) {
    $message = date('Y-m-d H:i:s') . ': ' . $e->getMessage() . "\n";
    file_put_contents('error-pdo-connection.txt', $message, FILE_APPEND | LOCK_EX);
    die('Site down');

}