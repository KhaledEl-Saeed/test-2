<?php
    require_once "db.php";
    $id =$_GET['id'];
    $sql = 'SELECT * FROM students WHERE id =:id';
    $statement = $dbh->prepare($sql);
    $statement->execute([':id'=> $id]);
    $students = $statement->fetch(PDO::FETCH_OBJ);

    if(isset($_POST['name']) && isset($_POST['email'])){
       $name = $_POST['name'];
       $email = $_POST['email'];
    }

    $sql = 'UPDATE students SET name=:name, email=:email WHERE id=:id';
    $statement = $dbh->prepare($sql);
    if($statement->execute([':name' => $name, ':email' => $email, ':id' =>$id])){
        header("location:/");
    }
    
?>



<?php require_once "header.php" ?>

<div class="container">
    <div class="card mt-5">
        <div class="card-header">
            <h2>Edit Students</h2>
        </div>
        <div class="card-body">
            <?php if(!empty($message)): ?>
                <div class="alert alert-success">
                    <?php echo $message?>
                </div>
            <?php endif; ?>

            <form method="post">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input value="<?= $students->name; ?>" type="text" name="name" id="name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">email</label>
                    <input value="<?= $students->email; ?>" type="email" name="email" id="email" class="form-control">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-info"> Edit Student</button>
                </div>

            </form>
        </div>
    </div>
</div>


 <?php require_once "footer.php" ?>