<?php
    require_once "db.php";
    $sql = 'SELECT * FROM students';
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $students = $statement->fetchAll(PDO::FETCH_OBJ);

    

?>

<?php require_once "header.php" ?>
<div class="container">
    <div class="card mt-5">
        <div class="class-header">
            <h2>All Students</h2>
        </div>

        <div class="class-body">
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
                <?php foreach($students as $students): ?>
                <tr>
                    <td><?= $students->id; ?></td>
                    <td><?= $students->name; ?></td>
                    <td><?= $students->email; ?></td>
                    <td>
                    <a href="edit.php?id=<?= $students->id?>" class="btn btn-info">Edit</a>
                    <a onclick="return confirm('Are you sure you want to delete this entry?')" href="delete.php?id=<?= $students->id?>" class="btn btn-danger">Delete</a>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
        </div>

    </div>
</div>

 
 <?php require_once "footer.php" ?>
 