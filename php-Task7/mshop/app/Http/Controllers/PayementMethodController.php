<?php

namespace App\Http\Controllers;

use App\PayementMethod;
use Illuminate\Http\Request;

class PayementMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.payementmethods.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.payementmethods.create',[
            'payementmethod'=> PayementMethod::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payementmethod = PayementMethod::create($request->all());
        return redirect(route('admin.payementmethods.index', $payementmethod));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayementMethod  $payementMethod
     * @return \Illuminate\Http\Response
     */
    public function show(PayementMethod $payementMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PayementMethod  $payementMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(PayementMethod $payementMethod)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayementMethod  $payementMethod
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayementMethod $payementMethod)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayementMethod  $payementMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayementMethod $payementMethod)
    {
        //
    }
}
