<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{   
    protected $fillable = [
        'total_amount',
        'description',
        'date',
        'client_id',
        'user_id'
        
    ];
    
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

}
