<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayementMethod extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];
}
