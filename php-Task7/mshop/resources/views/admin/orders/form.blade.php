@csrf
<div class="form-group">
<label for="name">total_amount</label>
<input type="number" name="total_amount" id="total_amount" class="form-control" value="{{ isset($order)?$order->total_amount : '' }}">
        @if($errors->first('total_amount'))
<br>
    <span class="alert alert-danger">
        { $errors->first('total_amount')}}
    </span>
         @endif
</div>


<div class="form-group">
<label for="name">description</label>
<input type="text" name="description" id="description" class="form-control" value="{{ isset($order)?$order->description : '' }}">
        @if($errors->first('description'))
<br>
    <span class="alert alert-danger">
        { $errors->first('description')}}
    </span>
         @endif
</div>


<div class="form-group">
<label for="name">date</label>
<input type="date" name="date" id="date" class="form-control" value="{{ isset($order)?$order->date : '' }}">
        @if($errors->first('date'))
<br>
    <span class="alert alert-danger">
        { $errors->first('date')}}
    </span>
         @endif
</div>


<div class="form-group">
<label for="name">client_id</label>
<input type="number" name="client_id" id="client_id" class="form-control" value="{{ isset($order)?$order->client_id : '' }}">
        @if($errors->first('client_id'))
<br>
    <span class="alert alert-danger">
        { $errors->first('client_id')}}
    </span>
         @endif
</div>

<div class="form-group">
<label for="name">user_id</label>
<input type="number" name="user_id" id="user_id" class="form-control" value="{{ isset($order)?$order->user_id : '' }}">
        @if($errors->first('user_id'))
<br>
    <span class="alert alert-danger">
        { $errors->first('user_id')}}
    </span>
         @endif
</div>

<div class="text-center">
    <button type="submit" class="btn btn-success">Save</button>
</div>