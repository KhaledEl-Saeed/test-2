@extends('layouts.app')

@section('title', 'All payementmethods')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Payment Methods</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/">admin </a></li>
              <li class="breadcrumb-item active">payement method</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="card card-body">
              <div class="row">
                  <div class="col text-center">
                    <h1>Choose your Payement Method</h1>
                  </div>
              </div>
              <div class="row">
                  <div class="col text-center">
                      <a href="{{route('admin.payementmethods.create')}}" class="btn btn-success">Choose</a>
                  </div>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
@endsection