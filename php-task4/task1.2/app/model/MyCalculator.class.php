<?php

class MyCalculator
{
    
    public $num1;
    public $num2;

    function Add ($num1, $num2)
    {
        return $num1 + $num2;
    }

    function Subtract ($num1, $num2)
    {
        return $num1 - $num2;
    }

    function Multiply ($num1, $num2)
    {
        return $num1 * $num2;
    }

    function Divide ($num1, $num2)
    {
        return $num1 / $num2;
    }
}