<?php

class Circle extends Shape
{
    const  SHAPE_TYPE = '2';
    protected $raduis;
    
    function __construct($raduis){  
        parent  :: __construct();
        $this->raduis = $raduis;
    }

    public function circleArea($raduis){
        $this->$raduis = $raduis;
        $circleArea = 3.14 * $raduis ^ 2;
    }

    public function getFullDescription(){
        return (Circle($id) . ':' . $name . '-' . $raduis);
    }
}