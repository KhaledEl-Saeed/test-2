<?php

require_once 'app/model/Shape.php';
require_once 'app/model/Rectangle.php';
require_once 'app/model/Circle.php';


if(empty($_GET['p'])){ 
    $page = 'home';
} else{
    $page = $_GET['p'];
}

switch ($page){
    case 'home':
        require_once 'pages/home.view.php';
    break;

    default:
        require_once 'pages/404.view.php';
}