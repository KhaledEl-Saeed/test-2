<?php

namespace App\Http\Controllers;

use App\subgroups;
use Illuminate\Http\Request;

class SubgroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subgroups.index' , ['subgroups' => subgroups::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( 'subgroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subgroup = subgroups::create($request->all());
        return redirect(route('subgroups.index', $subgroup));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subgroups  $subgroups
     * @return \Illuminate\Http\Response
     */
    public function show(subgroups $subgroups)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subgroups  $subgroups
     * @return \Illuminate\Http\Response
     */
    public function edit(subgroups $subgroup)
    {
        return view('subgroups.edit', compact('subgroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subgroups  $subgroups
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subgroups $subgroup)
    {
        $subgroup->update($request->all());
        return redirect(route('subgroups.index', $subgroup));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subgroups  $subgroups
     * @return \Illuminate\Http\Response
     */
    public function destroy(subgroups $subgroup)
    {
        $subgroup->delete();
        return redirect(route('subgroups.index'));
    }
}
