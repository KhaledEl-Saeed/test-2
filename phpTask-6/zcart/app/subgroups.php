<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subgroups extends Model
{
    protected $fillable = [
        'name',
        'description',
        'group_id'   
    ];
}
