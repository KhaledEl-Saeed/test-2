<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

    <title>Groups</title>
  </head>
  <body>
    
    
    <div class="container">
    <h1 class="mt-5"> Groups</h1>
    <a class="btn btn-success mt-2" href="{{route('groups.create')}}">Create new group</a>
    <table class="table mt-5">
        <thead>
            <tr>
            <th scope="col">Group_ID</th>
            <th scope="col">name</th>
            <th scope="col">desc.</th>
            <th scope="col"></th>
            <th scope="col"></th>
            </tr>
        </thead>
        @foreach($groups as $groups)
        <tbody>
            <tr>
            
                <th scope="row">{{$groups->id}}</th>
                <td>{{$groups->name}}</td>
                <td>{{$groups->description}}</td>
                <td><a class="btn btn-info" href="{{route('groups.edit', $groups)}}" >Edit</a>
                 <form action="{{ route('groups.destroy', $groups) }}" method="post" style="display: inline-block">
                 @csrf
                 @method('DELETE')
                 <button class="btn btn-danger">Delete</button>
                 </form> </td> 
            </tr> 
        </tbody>
        @endforeach
    </table>
    </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>